﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using MCTlib;
using NDesk.Options.Fork;
/*
 * This program manages pipegen queues. It gets a list of clients that haven't been run in the last 15 minutes, belonging to to the named queue,
 * and hands them to pipegen to run.
*/

namespace qmanager
{
    
    class Program
    {
        static bool verbose = false;
        static bool test = false;
        static string tld = "";
        static string queue = "";
        static bool show_help = false;
        static string assign = "";
        static string batch = "";

        static void Main(string[] args)
        {
            List<Tuple<int, string, bool>> queuelist = new List<Tuple<int, string, bool>>();

            var p = new OptionSet()
            {
                { "v|verbose", "verbose mode.", v=> { if (v != null) verbose = true; } },
                { "t|test", "test mode.", v=> { if (v != null) test = true; } },
                { "assign=", "Assign to a queue", v  => assign = v },
                { "batch=", "Batch file to run.", v => batch = v },
                { "h|help", "Show message and exit.", v => show_help = v != null }
            };
            
            List<string> extra = p.Parse(args);
            if (assign != "")
            {
                if (extra.Count != 1)
                {
                    Console.WriteLine("Please specify a client and a queue name");
                    p.WriteOptionDescriptions(Console.Out);
                    return;
                }
                string clientname = extra[0];
                AddClientToQueue(assign, clientname);
                return;
            }
            if (extra.Count != 2)
            {
                Console.WriteLine("I require queue name and configuration directory path as an argument");
                p.WriteOptionDescriptions(Console.Out);
                return;
            }
            

            if (show_help)
            {
                p.WriteOptionDescriptions(Console.Out);
                return;
            }
            
            queue = extra[0];
            if (verbose)
                Console.WriteLine("Process queue: {0} Configuration directory: {1}", queue, tld);
            queuelist = GetQueueList(queue);
            if (batch == "")
                batch = Properties.Resources.Batchjob;

            RunQueue(queuelist, queue, tld, batch);

        }
        static void RunQueue(List<string> queuelist, string queue, string tld, string batch)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            foreach (string q in queuelist)
            {
                Console.WriteLine("{0}", q);
                if (! test)
                {
                    ExecuteCommand(batch);
                }
            } 
            watch.Stop();
            TimeSpan ts = watch.Elapsed;
            Console.WriteLine("{0}:{0} elapsed mintes", ts.Minutes, ts.Seconds);
            UpdateRuntime((int) ts.TotalSeconds, queue);

            Console.ReadKey();

        }
        static bool AddClientToQueue(string queue, string clientname)
        {
            Tuple<int, string, bool> clientInfo = FindClientId(clientname);
            Tuple<int, string, bool> queueInfo = FindQueueId(queue);
            
            if (! AddToQueue(client_id, queue_id))
            {
                return false;
            }
            return true;
        }

        static Tuple<int, string, bool> FindClientId(string shortname)
        {
            IntegrationsDB db = new IntegrationsDB();
            db.Connect();
            string sql = @"SELECT shortname, id, status_flag FROM clients WHERE lower(shortname) = lower(@shortname)";
            BindArgs c = new BindArgs();
            c.add("@shortname", shortname);
            bool state = false;

            db.Query(sql, "client", @c);
            if (db.DT.Rows.Count == 0)
            {
                Console.WriteLine("{0} is not a client shortname defined in clients table", shortname);
                throw new Exception(shortname + " is not a valid client name");
            }

            
            DataRow d = db.DT.Rows[0];
            int status_flag = Convert.ToInt32(d["status_flag"]);
            if (status_flag == 1)
                state = true;
           
            Tuple<int, string, bool> info = new Tuple<int, string, bool>(Convert.ToInt32(d["id"]),d["shortname"].ToString(),state);

            return info;
        }
        static Tuple<int, string, bool> FindQueueId(string queue)
        {
            IntegrationsDB db = new IntegrationsDB();
            db.Connect();
            string sql = @"SELECT name, id, active FROM jobqs WHERE lower(name) = lower(@name)";
            BindArgs c = new BindArgs();
            c.add("@name", queue);

            db.Query(sql, "queue", @c);
            if (db.DT.Rows.Count == 0)
            {
                Console.WriteLine("{0} is not a queue name in jobqs table", queue);
                throw new Exception(queue + " is not a valid queue name");
            }
            DataRow d = db.DT.Rows[0];

            bool tf = true;

            if (Convert.ToInt32(d["active"]) == 0)
                tf = false;
            Tuple<int, string, bool> info = new Tuple<int, string, bool>(Convert.ToInt32(d["id"]), d["name"].ToString(), tf);
            return info;
        }
        static void UpdateRuntime(int seconds, string queue)
        {
            IntegrationsDB db = new IntegrationsDB();
            db.Connect();
            BindArgs b = new BindArgs();
            b.add("@seconds", seconds);

            string sql = "UPDATE jobqs SET runtime_seconds = @seconds";
            db.Query(sql, b);
        }
        static bool AddToQueue(int client_id, int queue_id)
        {
            IntegrationsDB db = new IntegrationsDB();
            db.Connect();
            BindArgs b = new BindArgs();
            b.add("@client_id", client_id);
            b.add("@queue_id", queue_id);
            string sql = "REPLACE INTO clients_jobqs (client_id, jobq_id) VALUES(@client_id, @queue_id)";
            db.Query(sql, b);
            return true;
        }
        static List<string> GetQueueList(string queue)
        {
            IntegrationsDB db = new IntegrationsDB();
            db.Connect();
            List<string> queuelist = new List<string>();

            Tuple<int, string, bool> queueInfo = FindQueueId(queue);
            BindArgs b = new BindArgs();
            b.add("@queue_id", queueInfo.Item1);
            string sql = "SELECT c.shortname, c.id, q.id, q.name as queue_name from clients c JOIN clients_jobqs jq on(jq.jobq_id = @queue_id) JOIN jobqs q on(q.id = jq.jobq_id) WHERE c.id = jq.client_id and jq.jobq_id = @queue_id";
            db.Query(sql, "queue", b);
            if (db.DT.Rows.Count == 0)
            {
                Console.WriteLine("Queue ({0}) is empty", queue);
            }
            foreach (DataRow d in db.DT.Rows)
            {
                queuelist.Add(d["shortname"].ToString());
            }
            return queuelist;
        }
        static void ExecuteCommand(string command)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;

            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");
            process.Close();

            sw.Stop();
            TimeSpan ts = sw.Elapsed;
            if (ts.TotalSeconds > 120)
            {
                Console.WriteLine("Client took {0} seconds to run!", ts.TotalSeconds);
            }
        }

    }
}
